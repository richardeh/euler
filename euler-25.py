import time
#fib=lambda n:n if n<2 else fib(n-1)+fib(n-2)

fib_term=2
fib_number=1

found=False
fib=[1,1,2]
i=0

start=time.time()
while not found:
    i=(i+1)%3
    fib_term+=1
    fib[i]=fib[(i+1)%3]+fib[(i+2)%3]
    if len(str(fib[i]))>1000:
        found=True
        
elapsed=time.time()-start

print(elapsed)

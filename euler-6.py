def sum_squares(a,b):
    total=0
    for i in range(a,b+1):
        total+=i**2
    return total

def square_sum(a,b):
    total=0
    for i in range(a,b+1):
        total+=i
    return total**2

sq,sumsq=sum_squares(1,100),square_sum(1,100)
print(sumsq-sq)

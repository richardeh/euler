"""

Using names.txt (right click and 'Save Link/Target As...'), a 46K text
file containing over five-thousand first names, begin by sorting it
into alphabetical order. Then working out the alphabetical value for
each name, multiply this value by its alphabetical position in the
list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN,
which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the
list. So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
"""

import time

def wordscore():
    LETTERS="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    with open('names.txt','r') as f_in, open('namessorted.txt','w') as f_out:
        line=f_in.readline()
        sline=line.split(",")
        for name in sorted(sline):
            f_out.write(name[1:len(name)-1]+"\n")

    line_count=0
    score=0

    with open('namessorted.txt','r') as f:
        for line in f:
            line_count+=1
            for c in line:
                if c is not "\n":
                    score+=(LETTERS.index(c.upper())+1)*line_count
    print(score)
    
start = time.time()
 
wordscore()

elapsed = time.time() - start
 
print(elapsed)

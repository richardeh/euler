def ispal(n):
    n=str(n)
    for i in range(len(n)):
        if n[i] is not n[(len(n)-i-1)]:
            return False
    return n

for i in range(900,999):
    for j in range(900,999):
        if str(ispal(i*j)).isdigit():
            print (i,j,i*j)


fib_val=0
turn=0
total=0
fib=lambda n:n if n<2 else fib(n-1)+fib(n-2)

while fib_val<4000000:
    fib_val=fib(turn)
    if fib_val%2 is 0:
       total+=fib_val
    turn+=1
    print(total,fib_val,turn)

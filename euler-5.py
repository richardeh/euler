def lcm(a,b):
    tmp=a
    while tmp%b:
        tmp+=a
    return tmp

def rec_lcm(a,b):
    for i in range(1,b):
        val=lcm(a,i)
        a=val
    return val

print (rec_lcm(1,20))
    

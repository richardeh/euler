"""

You are given the following information, but you may prefer to do some research for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
"""
def is_sunday(n,month):
    if n%7 is 0:
        print(month)
        return 1
    return 0

day=0
sundays=0
for year in range(1901,2001):
    # January
    day+=31
    sundays+=is_sunday(day,"february")

    # February
    if (year%4 is 0 and year %100 is not 0) or \
    (year % 100 is 0 and year % 400 is 0):
        #leap year
        day+=29
        print("leap year",year)
    else:
        day+=28
    sundays+=is_sunday(day,"march")
    
    # March
    day+=31
    sundays+=is_sunday(day,"april")

    # April
    day+=30
    sundays+=is_sunday(day,"may")
    
    # May
    day+=31
    sundays+=is_sunday(day,"june")
    
    # June
    day+=30
    sundays+=is_sunday(day,"july")
    
    # July
    day+=31
    sundays+=is_sunday(day,"august")

    # August
    day+=31
    sundays+=is_sunday(day,"september")

    # September
    day+=30
    sundays+=is_sunday(day,"october")

    # October
    day+=31
    sundays+=is_sunday(day,"november")

    # November
    day+=30
    sundays+=is_sunday(day,"december")

    # December
    day+=31
    sundays+=is_sunday(day,"january")
    
    print(year,sundays)

"""

If the numbers 1 to 5 are written out in words: one, two, three, four, five,
then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out
in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
20 letters. The use of "and" when writing out numbers is in compliance with
British usage.
"""

ONES=["zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
      "nine"]
TEENS=["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
       "seventeen", "eighteen", "nineteen"]
REST=["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty",
      "ninety", "hundred", "thousand"]

total=0
for i in range(1,1001):
    thousands=(i%10000)//1000
    hundreds=(i%1000)//100
    tens=(i%100)//10
    ones=i%10
    printstring=""
    if thousands:
        total+=len(ONES[thousands])+len(REST[9])

    if hundreds:
        total+=len(ONES[hundreds])+len(REST[8])
        if tens or ones:
            total+=len("and")

    if tens:
        if tens is 1:
            total+=len(TEENS[ones])
        else:
            total+=len(REST[tens-2])

    if ones and tens is not 1:
        total+=len(ONES[ones])

print(total)

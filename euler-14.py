"""


The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
"""

def get_next(n):
    if n%2 is 0:
        return int(n/2)
    else:
        return (3*n+1)

top=1000000
sequence=0
longest=0
the_num=0
for i in range(top,1,-1):
    sequence=0
    result=get_next(i)
    while result > 1:
        result=get_next(result)
        sequence+=1
    if sequence>longest:
        longest=sequence
        print(longest)
        the_num=i
print(longest)

"""
Find the sum of all the primes below two million.

Seive of Eratosthenes
"""

def sumprimes(n):
    sum = 0
    sieve = [True] * (n+1)
    for p in range(2, n):
        if sieve[p]:
            sum += p
            for i in range(p*p, n, p):
                sieve[i] = False
    return sum

print(sumprimes(2000000))

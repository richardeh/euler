from math import floor
import itertools

def factor(n):
    flatten_iter=itertools.chain.from_iterable
    results=set(flatten_iter((i,n//i) for i in range(1,int(n**0.5)+1) if n%i==0))
    if n in results:
        results.remove(n)
    return results

def isprime(n):
    for i in range(2,floor(n/2)):
        if n%i is 0:
            return False
    return True
